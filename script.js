var arrObj = [
  {
    name: "Krunal",
    age: 26,
    salary: "12"
  },
  {
    name: "malak",
    age: 23,
    salary: "12"
  }
];

display_array();
var text = "";
var editobject;

function reset() {
  document.getElementById("name").value = "";
  document.getElementById("age").value = "";
  document.getElementById("salary").value = "";
}
function presentAlert() {
  const controller = document.querySelector("ion-alert-controller");
  controller
    .create({
      header: "Confirmation",
      message: "Are you sure you want to add this employee?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            controller.dismiss(true);
            name = document.getElementById("name").value;
            age = document.getElementById("age").value;
            salary = document.getElementById("salary").value;

            console.log("name", name, "age", age, "salary", salary);

            arrObj.push({
              name: name,
              age: age,
              salary: salary
            });
            reset();
            console.log(arrObj);
            display_array();
          }
        },
        {
          text: "No",
          handler: () => {
            controller.dismiss(true);
            return false;
          }
        }
      ]
    })
    .then(alert => {
      alert.present();
    });

  //   return alert.present();
}

function display_array() {
  var text = `<ion-item><ion-col size='3'> <ion-label color="primary">  Name
  </ion-label> </ion-col> 
  <ion-col size='3'> <ion-label color="primary">Age
  </ion-label> </ion-col>
  <ion-col size='3'> <ion-label color="primary">Salary
  </ion-label> </ion-col>
  <ion-col size='3'>
 
  </ion-col> </ion-item><hr/>`;
  var Salaries = 0;
  fLen = arrObj.length;
  console.log("display array", arrObj);
  document.getElementById("employee").innerHTML = fLen;
  for (i = 0; i < fLen; i++) {
    Salaries += Number(arrObj[i].salary);
    text +=
      " <ion-item><ion-col size='3'> <ion-label>" +
      arrObj[i].name +
      "</ion-label> </ion-col> " +
      "<ion-col size='3'> <ion-label>" +
      arrObj[i].age +
      "</ion-label> </ion-col>" +
      "<ion-col size='3'> <ion-label>" +
      arrObj[i].salary +
      "</ion-label> </ion-col>" +
      "<ion-col size='3'>" +
      "<ion-icon name='create' onclick='edit(" +
      i +
      ")'></ion-icon>" +
      "<ion-icon name='trash' onclick='Delete(" +
      i +
      ")'></ion-icon>" +
      "</ion-col> </ion-item>";
  }
  document.getElementById("demo").innerHTML = text;
  document.getElementById("Totalsalary").innerHTML = Salaries;
}

customElements.define(
  "modal-page",
  class ModalContent extends HTMLElement {
    connectedCallback() {
      const modalElement = document.querySelector("ion-modal");
      console.log(
        "modalElement",
        modalElement,
        modalElement.componentProps.index
      );
      this.innerHTML =
        `
                <ion-header>
                <ion-toolbar>
                    <ion-title>Edit Employee</ion-title>
                    <ion-buttons slot="primary">
                    <ion-button onClick="dismissModal()">
                        <ion-icon slot="icon-only" name="close"></ion-icon>
                    </ion-button>
                    </ion-buttons>
                </ion-toolbar>
                </ion-header>
                <ion-content class="ion-padding">
                <ion-grid>
                <ion-row>
                  <ion-col size="12">

                  <ion-item>
                <ion-label position="floating">Name</ion-label>
                    <ion-input
                      name="name"
                      id="editname"
                      value='` +
        modalElement.componentProps.name +
        `'
                    ></ion-input>
                    </ion-item>
                  </ion-col>
      
                  <ion-col size="12">
                  <ion-item>
                <ion-label position="floating">Age</ion-label>
                   
                    <ion-input
                      name="age"
                      id="editage"
                      value='` +
        modalElement.componentProps.age +
        `'
                    ></ion-input>
                    </ion-item>
                  </ion-col>
                  <ion-col size="12">
                  <ion-item>
                <ion-label position="floating">Salary</ion-label>
                   
                    <ion-input
                      name="salary"
                      id="editsalary"
                      value='` +
        modalElement.componentProps.salary +
        `'
                    ></ion-input>
                    </ion-item>
                  </ion-col>
                 
                  <ion-col size="12">
                    <ion-button expand="full" color="primary" onclick="editform(` +
        modalElement.componentProps.index +
        `)"
                      >edit</ion-button
                    >
                   
                  </ion-col>
                 
                </ion-row> </ion-grid
            >
                </ion-content>`;
    }
  }
);
let modalElement = null;
const controllers = document.querySelector("ion-modal-controller");

function edit(index) {
  controllers
    .create({
      component: "modal-page"
    })
    .then(modal => {
      modal.present();
      modal.componentProps = {
        index: index,
        name: arrObj[index].name,
        age: arrObj[index].age,
        salary: arrObj[index].salary
      };
      modalElement = modal;
    });
}

function dismissModal() {
  if (modalElement) {
    modalElement.dismiss().then(() => {
      modalElement = null;
    });
  }
}

function editform(index) {
  name = document.getElementById("editname");
  age = document.getElementById("editage");
  salary = document.getElementById("editsalary");
  // Get the value
  var name = name.value;
  var age = age.value;
  var salary = salary.value;
  var myObj = {
    name: name,
    age: age,
    salary: salary
  };
  if (name) {
    console.log(index, arrObj);
    arrObj.splice(index, 1, myObj);
    console.log(index, arrObj);

    name.value = "";
    age.value = "";
    salary.value = "";
    dismissModal();
    display_array();
  }

  console.log(arrObj);
}
function Delete(index) {
  const controller = document.querySelector("ion-alert-controller");
  controller
    .create({
      header: "Confirmation",
      message: "Are you sure you want to delete this employee?",
      buttons: [
        {
          text: "Yes",
          handler: () => {
            controller.dismiss(true);

            arrObj.splice(index, 1);
            display_array();
          }
        },
        {
          text: "No",
          handler: () => {
            controller.dismiss(true);
            return false;
          }
        }
      ]
    })
    .then(alert => {
      alert.present();
    });
}
